import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={Link} to="/products">KnP</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						{/*The 'as' prop allows components to be treated as if they are different component gaining access to it's properties and functionalities*/}
						{/*The "to" prop is use in place of the "href" prop for providing the URL for the page.*/}
						{/*The "exect" prop is used to highlight the active NavLink component that matches the URL*/}
						<Nav.Link as={NavLink} to="/products" exact="true">Products</Nav.Link>
						{(user.id !== null) ? 

							<>
								<Nav.Link as={NavLink} to="/cart" exact="true">Add To Cart</Nav.Link>

								<Nav.Link as={NavLink} to="/orders" exact="true">Orders</Nav.Link>
								
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
						: 
							<>
								<Nav.Link as={Link} to="/">Register</Nav.Link>
								<Nav.Link as={Link} to="/login">Login</Nav.Link>
							</>
							}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}