import { Button } from 'react-bootstrap';
import { useEffect, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Checkout({retrieveCart}){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const checkout = (e) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_BASE_URL}/orders/checkout`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data.message !== 'Cart not found'){
				Swal.fire({

					title: 'We received you order!',
					icon: 'success'

				})

				retrieveCart()
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Cart is empty"
				})

				retrieveCart()
			}
			
		})
	}

	return(
		<Button variant="primary" onClick={e => checkout(e)}>Checkout</Button>
	)
}