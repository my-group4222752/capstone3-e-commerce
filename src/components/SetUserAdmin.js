import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2'

export default function SetUserAdmin(){

	const [show, setShow] = useState(false);

	const [userId, setUserId] = useState("")
	const handleShow = () => setShow(true);

	const handleClose = () => {
		setShow(false)

		setUserId("")
	};

	const setAdmin = (e, userId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_BASE_URL}/users/${userId}/set-as-admin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Berear ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
      
			if(data.message === "User updated as admin"){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: data.message
				})

				handleClose();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: data.message
				})

				handleClose();
			}
		})
	}

	return(
		<>
			<Button variant="secondary" size="sm" onClick={handleShow}>Set User as Admin</Button>

			<Modal show={show} onHide={handleClose}>
				<Form onSubmit={e => setAdmin(e, userId)}>
				    <Modal.Header closeButton>
				        <Modal.Title>Enter User Id</Modal.Title>
				    </Modal.Header>
				    <Modal.Body>
				    	<Form.Group controlId="productName">
			                <Form.Label>User Id</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	required
			                	value={userId}
			                	onChange={e => setUserId(e.target.value)}
			                	/>
			            </Form.Group>
				    </Modal.Body>
				    <Modal.Footer>
				        <Button variant="secondary" onClick={handleClose}>
				            Close
				        </Button>
				        <Button variant="primary" type="submit">
				            Set Admin
				        </Button>
				    </Modal.Footer>
			    </Form>
			</Modal>
		</>
	)
}