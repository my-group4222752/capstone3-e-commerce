import {useState, useContext} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct(){

	const {user} = useContext(UserContext);

	const [showEdit, setShowEdit] = useState(false);

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");

	function createProduct(e){
		e.preventDefault();

		let token = localStorage.getItem('token');
		console.log(token);

		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/`,{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price

			})
		})
		.then(res => res.json())
		.then(data => {

			//data is the response of the api/server after it's been process as JS object through our res.json() method.
			console.log(data);

			if(data.error === "Product already exists"){
				

				Swal.fire({

					icon: "error",
					title: "Product already exists.",
					text: data.message

				})

				closeEdit();

			} else if (data.error === "Failed to save the product") {

				Swal.fire({

					icon: "error",
					title: "Unsuccessful Product Creation",
					text: data.message

				})

				closeEdit();

			} else {

				Swal.fire({

					icon:"success",
					title: "Product Added"

				})

				closeEdit();

			}

		})

		setName("")
		setDescription("")
		setPrice(0);
	}

	const openEdit = (courseId) => {

		// open the modal
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit()}> Add Product </Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<h1 className="my-5 text-center">Add Product</h1>
				<Form onSubmit={e => createProduct(e)}>
					<Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    	<Form.Group>
						<Form.Label>Name:</Form.Label>
						<Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
						</Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                    	<Button variant="primary" type="submit" className="my-5">Submit</Button>
                    </Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}