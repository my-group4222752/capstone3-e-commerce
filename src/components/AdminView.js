import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import ArchiveProduct from './ArchiveProduct';
import UpdateProduct from './UpdateProduct';
import AddProduct from '../components/AddProduct';
import SetUserAdmin from './SetUserAdmin'

export default function AdminDashboard({productsData, retrieveProducts}){

	const [products, setProducts] = useState([]);

	// const [showProducts, setShowProducts] = useState(false);

	// const handleButtonClick = () => {
	// 	setShowProducts(true);
	// };

	useEffect(() => {
		const productsArr = productsData.map(product => {
			console.log(product);

			return (
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
					{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td><UpdateProduct products={product._id} retrieveProducts={retrieveProducts} /></td>
					<td><ArchiveProduct className="btn btn-danger" product={product._id} isActive={product.isActive} retrieveProducts={retrieveProducts}/></td>
				</tr>
			)
		})

		setProducts(productsArr)

	}, [productsData])

	return(
		<>
			<h1 className="text-center my-4"> Admin Dashboard</h1>
			<div className="d-flex justify-content-center my-4">
				<AddProduct />
        		<SetUserAdmin />
			</div>
			<Table striped bordered hover responsive>
				<thead>
					<tr className="text-center">
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan="2">Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
		</>

	)

}