import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateProduct({products, retrieveProducts}) {
	
	const [ productId, setProductId] = useState("");
	const [ productName, setProductName] = useState("");
	const [ productDescription, setProductDescription] = useState("");
	const [ productPrice, setProductPrice] = useState("");
	const [ showUpdate, setShowUpdate] = useState(false);

	const openUpdate = (productId) => {
		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

			setProductId(data.product._id);
			setProductName(data.product.name);
			setProductDescription(data.product.description);
			setProductPrice(data.product.price);
		})

		setShowUpdate(true);
	}

	const closeUpdate = () => {
		setShowUpdate(false);

		setProductId("");
		setProductName("");
		setProductDescription("");
		setProductPrice(0);
	}

	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/${ productId }/update`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.message === "Product updated successfully"){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: data.message
				})

				closeUpdate();

				retrieveProducts();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: data.message
				})

				closeUpdate();

				retrieveProducts();
			}
		})
	}


	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openUpdate(products)}>Update</Button>

			<Modal show={showUpdate} onHide={closeUpdate}>
			    <Form onSubmit={e => editProduct(e, productId)}>
			        <Modal.Header closeButton>
			            <Modal.Title>Edit Course</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>    
			            <Form.Group controlId="productName">
			                <Form.Label>Name</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	required
			                	value={productName}
			                	onChange={e => setProductName(e.target.value)}
			                	/>
			            </Form.Group>
			            <Form.Group controlId="productDescription">
			                <Form.Label>Description</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	required
			                	value={productDescription}
			                	onChange={e => setProductDescription(e.target.value)}
			                	/>
			            </Form.Group>
			            <Form.Group controlId="productPrice">
			                <Form.Label>Price</Form.Label>
			                <Form.Control 
			                	type="number" 
			                	required
			                	value={productPrice}
			                	onChange={e => setProductPrice(e.target.value)}
			                	/>
			            </Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			            <Button variant="secondary" onClick={closeUpdate}>Close</Button>
			            <Button variant="success" type="submit">Submit</Button>
			        </Modal.Footer>
			    </Form>
			</Modal>
		</>
	)
} 
