import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, retrieveProducts}){

	const archiveToggle = (productId) => {
		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/${productId}/archive`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.message === 'Product has been archived') {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				retrieveProducts();

			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				retrieveProducts();
			}


		})
	}


	const activateToggle = (productId) => {
		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/${productId}/active`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.message === "Product has been activated") {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully enabled'
				})
				retrieveProducts();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				retrieveProducts();
			}


		})
	}

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>

			}
		</>

	)
}