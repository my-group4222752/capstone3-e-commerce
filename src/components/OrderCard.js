import React, { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function OrderCard({orderData}){

	const [orders, setOrders] = useState([]);

	useEffect(() => {

		const orderArr = orderData.map(order => {
			console.log("order:", order)

			const productArr = order.productsOrdered.map(product => {

				return(
					<li key={product._id}>{product.productId} - Quantity {product.quantity}</li>
				)
			})

			return(
				<Card>
					<Card.Body>
						<Card.Title>Order placed on: {order.orderedOn.slice(0, 10)}</Card.Title>
						<Card.Subtitle>Status: {order.status}</Card.Subtitle>
						<Card.Text>
						<ul>
							{productArr}
						</ul>
						Total: &#x20B1; {order.totalPrice}
						</Card.Text>
					</Card.Body>
				</Card>
			)
		})

		setOrders(orderArr)

	}, [orderData])

	return(
		<>
			{orders}
		</>
	)

}