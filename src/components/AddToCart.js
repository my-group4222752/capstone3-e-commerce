import { Button } from 'react-bootstrap';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AddToCart({quantity, price, productId}){

  const navigate = useNavigate();
	let subtotal = quantity * price;

	function addCart(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_BASE_URL}/cart/add-to-cart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			

			if(data){
				Swal.fire({

					title: 'Successfully Added',
					icon: 'success',
					text: 'You have successfully added to cart.'

				})
        
        navigate("/cart");
        
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				});
			}

		})
	}


	return(
		<Button variant="primary" onClick={e => addCart(e)}>Add to Cart</Button>
	)
}
