import { Card} from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import OrderCard from '../components/OrderCard';

export default function Orders(){

	const [orders, setOrders] = useState([]);
	const [products, setProducts] = useState([]);

	const getOrder = () => {

		fetch(`${process.env.REACT_APP_API_BASE_URL}/orders/my-orders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data !== undefined){
				setOrders(data.existingOrder)
			} else {
				setOrders([])
			}
		})
	}	

	useEffect(() => {
		getOrder()
	}, [])

	return(
		<>
			<h1 className="text-center">Order History</h1>
			<OrderCard orderData={orders} />
		</>
	)
}