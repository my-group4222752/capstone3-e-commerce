import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { useContext, useState, useEffect } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Products(){

	const {user} = useContext(UserContext);

	const [products, setProducts] = useState([]);

	const retrieveProducts = () => {

		let productsUrl = user.isAdmin ? `${process.env.REACT_APP_API_BASE_URL}/products/all` : `${process.env.REACT_APP_API_BASE_URL}/products`;

		fetch(productsUrl, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(typeof data !== 'undefined'){
				setProducts(data.products);
			} else {
				setProducts([]);
			}
		})


	}
		

	useEffect(() => {
		retrieveProducts()

	}, [])

	return(
		<>
			{
				(user.isAdmin === true) ?
					// Pass the fetchData as a props
					<AdminView productsData={products} retrieveProducts={retrieveProducts}/>

				:

					<UserView productsData={products} />
			}
		</>
	)
}