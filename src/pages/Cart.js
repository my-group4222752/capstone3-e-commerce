import { Table} from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Checkout from '../components/Checkout';
import Swal from 'sweetalert2';

export default function Cart(){
	
	const [total, setTotal] = useState(0);
	const [cartItems, setcartItems] = useState([]);

	const retrieveCart = () => {
		fetch(`${process.env.REACT_APP_API_BASE_URL}/cart/get-cart`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.cartItems)
			setTotal(data.totalPrice)

			if(data.cartItems !== undefined){
				const cartArr = data.cartItems.map(item => {
					console.log(item)

					return (
						<tr key={item.productId}>
							<td>{item.productId}</td>
							<td>{item.quantity}</td>
							<td>{item.subtotal}</td>
						</tr>
					)
				})

				setcartItems(cartArr);
			} else {
                Swal.fire({
                    title: "Cart Empty",
                    icon: "error",
                    text: "Cart is currently empty please add to cart"
                });

                setcartItems([])
            }

		})
	}


	useEffect(() => {
		retrieveCart()
	}, [])


	return(
		<>
			<h1 className="text-center mt-3">Your Shopping Cart</h1>
			<Table striped bordered hover responsive>
				<thead>
					<tr className="text-center">
						<th>Product Id</th>
						{/*<th>Price</th>*/}
						<th>Quantity</th>
						<th>Subtotal</th>
					</tr>
				</thead>

				<tbody>
					{cartItems}
					<tr>
						<td colspan="2"><Checkout retrieveCart={retrieveCart} /></td>
						<td>Total: {total}</td>
					</tr>
				</tbody>
			</Table>
		</>
	)
}