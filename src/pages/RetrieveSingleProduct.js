import { useEffect, useState, useContext } from 'react';
import { Container, Col, Row, Card, Button} from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import AddToCart from '../components/AddToCart'

export default function RetrieveSingleProduct(){

	const { productId } = useParams();

	const [productName, setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [productPrice, setProductPrice] = useState("");
	const [productQuantity, setProductQuantity] = useState(1);
	const navigate = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_BASE_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductName(data.product.name);
			setProductDescription(data.product.description);
			setProductPrice(data.product.price);

		})
	}, [])

	return(
		<>
			<h1 className="text-center mt-3">Product Details</h1>
			<Container className="mt-5">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card border="dark">
						    <Card.Header className="text-center fw-bold">{productName}</Card.Header>
						    <Card.Body>
						    	<Card.Subtitle>Description:</Card.Subtitle>
						        <Card.Text>{productDescription}</Card.Text>
						        <Card.Subtitle>Price:</Card.Subtitle>
						        <Card.Text>PhP {productPrice}</Card.Text>
						        <Card.Subtitle>Quantity:</Card.Subtitle>
						        <Card.Text><input
						        	type="number"
						        	value={productQuantity}
						        	onChange={e => {setProductQuantity(e.target.value)}}/>
						        </Card.Text>
						        <Card.Footer className="text-center"><AddToCart quantity={productQuantity} price={productPrice} productId={productId}/></Card.Footer>
						    </Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		</>
	)
}