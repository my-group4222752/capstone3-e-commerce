import {Form, Button, Col} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';

function Login() {
  
  const {user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(true);

  function loggingIn(e){
  	e.preventDefault();
  	fetch(`http://localhost:4000/users/login`, {
  		method: 'POST',
  		headers: {
  			"Content-Type": "application/json"
  		},
  		body: JSON.stringify({
  			email: email,
  			password: password
  		})
  	})
  	.then(res => res.json())
  	.then(data => {
  		console.log(data)
  		if(typeof data.access !== "undefined"){

  		    localStorage.setItem('token', data.access);
  		    userDetails(data.access);

  		    Swal.fire({
  		        title: "Login Successful",
  		        icon: "success"
  		    })
  		                
  		} else if (data.error === "No Email Found") {

  		    Swal.fire({
  		        title: "Email not found.",
  		        icon: "error",
  		        text: "Check the email  you provided."
  		    })

  		} else {

  		    Swal.fire({
  		        title: "Authentication failed",
  		        icon: "error",
  		        text: "Check your login details and try again."
  		    })
  		}
  	})

  	setEmail('');
  	setPassword('');


  }

  const userDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_BASE_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data.user._id,
                isAdmin: data.user.isAdmin
            })
        })
    };

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

  return (

        (user.id !== null) ?
            <Navigate to="/products" />
        :
            <Form className="my-5" onSubmit={(e) => loggingIn(e)}>
            	<h1 className="my-5 text-center">Login</h1>
            	<Form.Group className="mb-3" controlId="formGroupEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    />
            	</Form.Group>
              	<Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)} 
                    required
                    />
              	</Form.Group>


            	<Form.Group className="mb-3 text-center">
                    <Col>
                    	{isActive ?
                    		<Button type="submit">Sign in</Button>
                    	:
                    		<Button type="submit" disabled>Sign in</Button>
                    	}
                    </Col>
            	</Form.Group>
            </Form>
    );
}

export default Login;