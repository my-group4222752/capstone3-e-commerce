import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function Register(){
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault();

		fetch(`http://localhost:4000/users/`,{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
	        		
			if(data.message === "User Registered Successfully"){

				setEmail('');
				setPassword('');
				setConfirmPassword('');

				Swal.fire({
	                title: "Registration Successful!",
	                icon: "success"
	            })

			} else if (data.error === "Email invalid") {

				Swal.fire({
	                title: "Email Invalid",
	                icon: "error"
	            })

			} else if (data.error === "Password must be atleast 8 characters") {

				Swal.fire({
	                title: "Password must be at least 8 characters",
	                icon: "error"
	            })

			} else {

				Swal.fire({
	                title: "Something went wrong.",
	                icon: "error"
	            })

			}
		})
		.catch(error => {
			console.error('Error:', error);
		});
	}

	useEffect(() => {
		if((email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){

			setIsActive(true)	

		} else {
			setIsActive(false)
		}
	}, [email, password, confirmPassword]);

	return(
		<>
			<Form onSubmit={(e) => registerUser(e)}>
				<h1 className="my-5 text-center">Register</h1>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control 
					type="text" 
					placeholder="Enter Email" 
					required
					value={email}
					onChange={e => {setEmail(e.target.value)}}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-1">Password:</Form.Label>
					<Form.Control 
					type="password" 
					placeholder="Enter Password" 
					required
					value={password}
					onChange={e => {setPassword(e.target.value)}}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-1">Confirm Password:</Form.Label>
					<Form.Control 
					type="password" 
					placeholder="Confirm Password" 
					required
					value={confirmPassword}
					onChange={e => {setConfirmPassword(e.target.value)}}
					/>
				</Form.Group>
				{ isActive ?
				<Button className="mt-1" variant="primary" type="submit">Submit</Button>
				:
				<Button className="mt-1" variant="primary" type="submit" disabled>Submit</Button>
				}
			</Form>	
			<p>Already have an account? Click <Link to="/login">here</Link> to login.</p>
		</>
	)
}